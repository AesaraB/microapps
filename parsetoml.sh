#!/usr/bin/env sh

# THIS IS A WIP, DO NOT USE IN PRACTICE

function help_prompt {
	cat <<- EOM
Usage:
	parser.sh [options] [file]

Options:
	-h, --help		Print this help message
	-t, --tables	List all tables in file
EOM
}

function cleanexp {
	sed -e '/^[ 	]*$/d;/[ 	]+$/d;/^[ 	]+/d'
}

function tables {
	grep -Pe '\[.*\]' $FILE
}

# What if no match is found?
# What if there's more than one match for the table name?
# What if the table is the last in the file?
function keyvaluepairs_in_table {
	table_line=$(grep -Pne "\\[$TABLE\\]" $FILE|cut -d ":" -f 1)
	table_start=$(($table_line+1))
	table_end=$(sed -ne "${table_start},\$P" $FILE | grep -Pnm 1 -e '\[.*\]' |cut -d ":" -f 1)
	table_end=$(($table_line+$table_end-1))
	sed -ne "$table_start,${table_end}P" $FILE | cleanexp
}
function keys_in_table {
	keyvaluepairs_in_table | cut -d "=" -f 1 | cleanexp
}
function keyvalue_in_table {
	keyvaluepairs_in_table | grep -Pe "$KEY[ 	]*=" | cut -d "=" -f 2 | cleanexp
	# if value begins with """
#		multilinestring=$(grep -Pnm 1 -e '"""'
}

# This only works if the value is a single line
function editkeyvalue_in_table {
	sed '${keyline}s/^\([ 	]*$KEY[ 	]*=[ 	]*\).*/\1$NEWVALUE/' $FILE
}

FILE=${@: -1}

while getopts "hkpst:v:" option; do
	case $option in
		h) # display Help
	 		help_prompt
			exit 0
			;;
		k)
			keys_in_table
			exit 0
			;;
		p)
			keyvaluepairs_in_table
			exit 0
			;;
		s)
			tables
			exit 0
			;;
		t)
			TABLE=$OPTARG
			;;
		v)
			KEY=$OPTARG
			keyvalue_in_table
			exit 0
			;;
	esac
done
