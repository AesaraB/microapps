# Aesara's microapps
A collection of assorted scripts and small applications that are too simple to warrant their own repository.

## Symlinking advice
When symlinking these files to a path, use absolute directories.
