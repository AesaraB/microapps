#!/bin/env bash

#	This script creates a file "$QOTD_CACHE"

SCRIPT_DIR=$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)
QOTD_CACHE=$SCRIPT_DIR/QOTD-cache
DATE=$(date --date='TZ="Etc/GMT+5"' +"%B_%-d,_%Y")

checkQOTD(){
	if [[ -f "$QOTD_CACHE" ]]; then
		QOTD_PREV_DATE=$(sed -n '1p' $QOTD_CACHE)
		if [[ $DATE == $QOTD_PREV_DATE ]]; then
			displayQOTD
		else
			buildQOTD
		fi
	else
		buildQOTD
	fi
}

buildQOTD() {
	getQOTD
	formatQOTD
	displayQOTD
}

getQOTD(){
	QOTD=$(curl -Ss "https://en.wikiquote.org/wiki/Wikiquote:Quote_of_the_day/${DATE}?action=raw")
	echo "$DATE" > $QOTD_CACHE
	echo "0" >> $QOTD_CACHE
}

formatQOTD(){
	QOTD_TEXT=$(echo "$QOTD" | grep "quote = "|cut -c"29-"| sed 's/<br\/>/\n/g' | perl -pe 's/\[\[([^\]]*?\|)?(.*?)\]\]/\2/g')
	QOTD_AUTHOR=$(echo "$QOTD" | grep "author = " | cut -c"11-")
	echo "$QOTD_TEXT

- $QOTD_AUTHOR" >> $QOTD_CACHE
}

displayQOTD(){
	qotd_displayed=$(sed -n '2p' $QOTD_CACHE)
	if [ $qotd_displayed -le 3 ]; then
		sed -n '3,$p' $QOTD_CACHE | cowsay -f bunny
		((qotd_displayed++))
		sed -i "2s/.*/$qotd_displayed/"  $QOTD_CACHE
	fi
}

checkQOTD
